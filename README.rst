An installer for the SODAT cybersecurity software.

Usage::

    curl -fsSL https://sodat.gaussalgo.com/install.sh -o /tmp/install.sh && sh -e /tmp/install.sh
