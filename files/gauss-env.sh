ENV_FILE="/etc/default/gauss"

source "$ENV_FILE"
export $(grep '^[A-Z]' "${ENV_FILE}" | cut -d= -f1)
