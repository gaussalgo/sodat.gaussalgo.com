#!/bin/sh

set -e

REPO_URL="https://sodat.gaussalgo.com/"
REPO_FILE_PREFIX="${REPO_URL}"
DOCKER_REGISTRY="docker.gaussalgo.com:443/sodatsw"
DOCKER_USERNAME="sodatsw-ro"
DOCKER_COMPOSE_URL="https://github.com/docker/compose/releases/download/1.8.0/docker-compose-$(uname -s)-$(uname -m)"
DOCKER_COMPOSE_SHA256="ebc6ab9ed9c971af7efec074cff7752593559496d0d5f7afb6bfd0e0310961ff"
DOCKER_GC_URL="https://raw.githubusercontent.com/spotify/docker-gc/e3f7fc45cba83b06b1cf985229c9d88f0ebc62c7/docker-gc"
CONFIG_DIR="/etc/default"
CONFIG_FILE="${CONFIG_DIR}/gauss"
ENV_SCRIPT="/opt/gauss/gauss-env.sh"
CURL="curl -fsSL"


check_root() {
    if [ "$(id -u)" != "0" ]
    then
        echo "This script must be run as root." >&2
        exit 1
    fi
}


install_docker() {
    yum update -y

    tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

    yum install -y docker-engine
    systemctl enable docker
    systemctl start docker
}


check_docker_auth() {
    if [ ! -f ~/.docker/config.json ]
    then
        docker_login_cmd="docker login -u ${DOCKER_USERNAME} ${DOCKER_REGISTRY}"
        echo "Docker authentication data not found." >&2
        echo "Running ${docker_login_cmd}"
        while true
        do
            $docker_login_cmd && return
        done
    fi
}


install_docker_compose() {
    if ! (echo "${DOCKER_COMPOSE_SHA256}  /usr/local/bin/docker-compose" | sha256sum --check --status)
    then
        curl -L "${DOCKER_COMPOSE_URL}" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
    fi
    docker-compose --version
}


install_config_file() {
    mkdir -p "${CONFIG_DIR}"
    if [ ! -f "${CONFIG_FILE}" ]
    then
        echo "Creating config file template: ${CONFIG_FILE}"
        ${CURL} "${REPO_FILE_PREFIX}/default/gauss" -o "${CONFIG_FILE}"
        echo "Please edit ${CONFIG_FILE} and set your database URL."
    fi
    ${CURL} "${REPO_FILE_PREFIX}/gauss-env.sh" -o "${ENV_SCRIPT}"
    . "${ENV_SCRIPT}"
}


install_gauss_service() {
    mkdir -p /opt/gauss
    mkdir -p /var/log/gauss

    ${CURL} "${REPO_FILE_PREFIX}/docker-compose.yml" -o /opt/gauss/docker-compose.yml
    ${CURL} "${REPO_FILE_PREFIX}/systemd/gauss.service" -o /etc/systemd/system/gauss.service
    ${CURL} "${REPO_FILE_PREFIX}/systemd/start.sh" -o /opt/gauss/start.sh
    ${CURL} "${REPO_FILE_PREFIX}/systemd/stop.sh" -o /opt/gauss/stop.sh
    chmod +x /opt/gauss/start.sh
    chmod +x /opt/gauss/stop.sh

    systemctl enable gauss
}


pull_docker_images() {
    docker-compose --project-name gauss --file /opt/gauss/docker-compose.yml pull
}


install_docker_gc() {
    ${CURL} "${DOCKER_GC_URL}" -o /usr/local/bin/docker-gc
    chmod +x /usr/local/bin/docker-gc

    cat >/opt/gauss/docker-gc-exclude <<EOF
${GA_BACKEND_IMAGE}:${GA_BACKEND_VERSION}
${GA_GUI_IMAGE}:${GA_GUI_VERSION}
postgres:9.5
redis:3
EOF

    cat >/etc/cron.hourly/docker-gc <<EOF
export GRACE_PERIOD_SECONDS=86400
export FORCE_IMAGE_REMOVAL=1
export EXCLUDE_FROM_GC=/opt/gauss/docker-gc-exclude
/usr/local/bin/docker-gc
EOF
    chmod +x /etc/cron.hourly/docker-gc
}


check_root
install_docker
check_docker_auth
install_docker_compose
install_config_file
install_gauss_service
pull_docker_images
install_docker_gc
