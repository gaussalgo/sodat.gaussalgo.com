#!/bin/bash

LOGFILE=/var/log/gauss/trace

log() {
    MSG="$1"
    >&2 echo $MSG
    echo $MSG >> $LOGFILE
}

log "START stop.sh"

docker ps -a | grep -P '(?!\s)gauss_.*?\b' -o  | xargs --no-run-if-empty --max-lines 1 docker stop
docker ps -a | grep -P '(?!\s)gauss_.*?\b' -o  | xargs --no-run-if-empty docker rm -f

log "EXIT stop.sh"
