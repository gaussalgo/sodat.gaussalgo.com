#!/bin/sh

set -e

LOGFILE=/var/log/gauss/trace

log() {
    MSG="$1"
    >&2 echo $MSG
    echo $MSG >> $LOGFILE
}

log "START start.sh"

cleanup() {
    /opt/gauss/stop.sh
}

# SIGINT   2
# SIGQUIT  3
# SIGTERM 15

trap 'cleanup; exit' SIGINT SIGTERM SIGQUIT

cleanup
log "GOING TO DOCKER-COMPOSE"
docker-compose --project-name gauss --file /opt/gauss/docker-compose.yml run --rm import gauss-admin --wait initdb >> $LOGFILE 2>&1
docker-compose --project-name gauss --file /opt/gauss/docker-compose.yml up >> $LOGFILE 2>&1
log "EXIT start.sh"
